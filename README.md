## Apuntes

**Spring3 (boot, mvc) + gradle + CORS + JSON & XML (request & response)**


Generar objeto propio para devolver listado de objetos en XML (ver Items object). Este objeto se puede representar en json.

Un listado de objetos (List por ejemplo) Item no puede ser representado en XML (error 406 Not Acceptable)

Algunos métodos expuestos soportan tanto JSON como XML **y** otros solo exponen respuesta en JSON **o** XML. En las peticiones hay que indicar la cabecera **Accept** con el valor correspondiente (application/json o application/xml) para jugar con las respuestas. Si no se le indica nada devuelve el tipo de respuesta por defecto, en el caso de que tenga un tipo de formato de vuelta seria el que se indica y si tuviera 2 la respuesta por defecto seria JSON.

****************************************************************************************************
**ERROR**

**Cuando realizamos build model en ggts: could not reserve enough space for object heap**

Lo resolvemos insertando en el fichero gradel.properties el siguiente valor:

`org.gradle.jvmargs=-Xms128m -Xmx256m`
****************************************************************************************************

**CURL**

POST

`curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"id":26,"name":"item 26"}' http://localhost:9000/api/items`

> `{"id":26,"name":"item 26"}`


`curl -v -H "Accept: application/xml" -H "Content-type: application/xml" -X POST -d '<item><id>23</id><name>Item 23</name></item>' http://localhost:9000/api/items`

> `<?xml version="1.0" encoding="UTF-8" standalone="yes"?><item><id>23</id><name>Item 23</name></item>`



GET

`curl -v -H "Accept: application/json" http://localhost:9000/api/items`

> `[{"id":1,"name":"item 1"},{"id":2,"name":"item 2"},{"id":3,"name":"item 3"},{"id":4,"name":"item 4"},{"id":5,"name":"item 5"},{"id":6,"name":"item 6"}]`


`curl -v http://localhost:9000/api/items`

> `[{"id":1,"name":"item 1"},{"id":2,"name":"item 2"},{"id":3,"name":"item 3"},{"id":4,"name":"item 4"},{"id":5,"name":"item 5"},{"id":6,"name":"item 6"}]`


`curl -v -H "Accept: application/xml" http://localhost:9000/api/itemsInXML`

> `<?xml version="1.0" encoding="UTF-8" standalone="yes"?><items><item><id>1</id><name>item 1</name></item><item><id>2</id><name>item 2</name></item><item><id>3</id><name>item 3</name></item><item><id>4</id><name>item 4</name></item><item><id>5</id><name>item 5</name></item><item><id>6</id><name>item 6</name></item></items> `


`curl -v http://localhost:9000/api/itemsInXMLXml`

> `<?xml version="1.0" encoding="UTF-8" standalone="yes"?><items><item><id>1</id><name>item 1</name></item><item><id>2</id><name>item 2</name></item><item><id>3</id><name>item 3</name></item><item><id>4</id><name>item 4</name></item><item><id>5</id><name>item 5</name></item><item><id>6</id><name>item 6</name></item></items> `


`curl -v -H "Accept: application/json" http://localhost:9000/api/itemInXML`

> `{"id":6,"name":"item 6"}`


`curl -v -H "Accept: application/xml" http://localhost:9000/api/itemInXML`

> `<?xml version="1.0" encoding="UTF-8" standalone="yes"?><item><id>6</id><name>item 6</name></item>`


`curl -v -H "Accept: application/xml" http://localhost:9000/api/items`

> `HTTP/1.1 406 Not Acceptable`


