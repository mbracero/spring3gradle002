package org.mbracero.controller;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpServletResponse;

import org.mbracero.model.Item;
import org.mbracero.model.Items;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ItemController extends BaseController {
	@SuppressWarnings("serial")
	private List<Item> items = new CopyOnWriteArrayList<Item>() {{
		add(new Item(1l, "item 1"));
		add(new Item(2l, "item 2"));
		add(new Item(3l, "item 3"));
		add(new Item(4l, "item 4"));
		add(new Item(5l, "item 5"));
		add(new Item(6l, "item 6"));
	}};
	
	@RequestMapping(value="/items", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public @ResponseBody List<Item> getItems() {
		return items;
	}
	
	@RequestMapping(value="/itemsInXML", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public @ResponseBody Items getItemsXML() {
    	return new Items(items);
	}
	
	@RequestMapping(value="/itemInXML", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public @ResponseBody Item getItemXML() {
		return new Item(6l, "item 6");
	}
	
	@RequestMapping(value="/itemsJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Item> getItemsOnlyJson() {
		return items;
	}
	
	@RequestMapping(value="/itemsInXMLJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Items getItemsXMLOnlyJson() {
    	return new Items(items);
	}
	
	@RequestMapping(value="/itemInXMLJson", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Item getItemXMLOnlyJson() {
		return new Item(6l, "item 6");
	}
	
	@RequestMapping(value="/itemsXml", method=RequestMethod.GET, produces=MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody List<Item> getItemsOnlyXml() {
		return items;
	}
	
	@RequestMapping(value="/itemsInXMLXml", method=RequestMethod.GET, produces=MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Items getItemsXMLOnlyXml() {
    	return new Items(items);
	}
	
	@RequestMapping(value="/itemInXMLXml", method=RequestMethod.GET, produces=MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Item getItemXMLOnlyXml() {
		return new Item(6l, "item 6");
	}
	
	@RequestMapping(value="/items", method=RequestMethod.POST)
	public @ResponseBody Item addItem(
			@RequestBody Item item,
			HttpServletResponse response) {
		
		items.add(item);
		
		response.addHeader("Location", "/api/items/" + item.getId());
		return item;
	}
}
