package org.mbracero.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="items")
public class Items{
    protected List<Item> list;

    public Items(){}

    public Items(List<Item> list){
    	this.list=list;
    }
    
    @XmlElement(name="item")
	public List<Item> getList() {
		return list;
	}
	public void setList(List<Item> list) {
		this.list = list;
	}   
}
